jQuery(document).ready(function(){
    initSlider();
	phoneNumberButton();

    jQuery('.all-check-place .check-place').click(function(){
    	jQuery(this).toggleClass('checked');
    	if(jQuery(this).hasClass('checked')){
    		jQuery(this).children('input').attr('checked','checked');
    		jQuery(this).children('input')[0].checked = true;
    	}else jQuery(this).children('input').removeAttr('checked');
    	search('');
    });

	$('#searchForm').submit(function(){
		return false;
	});

	$('.content').on('click', '.pager-place a, .buttom-pager a', function(){
		search($(this).attr('href'));
		return false;
	});

	$('.content').on('change', '#searchSelectOrder', function(){
		var v = $(this).val();
		search('?select=' + v, v);
		return false;
	});

	$('.search-place .clean-form input').click(function(){
		$('.search-place .check-place').removeClass('checked');
		$('.search-place input').removeAttr('checked');
		$('#slider-range-cost, #slider-range-s').slider('destroy');
		initSlider();
		setTimeout(function(){
			search('');
		}, 100);
	});
});


function initSlider()
{
    jQuery( "#slider-range-cost" ).slider({
      range: true,
      min: 0,
      max: 9000,
      values: [ 0, 9000 ],
      slide: function( event, ui ) {
      	jQuery('.cost_min input').val(ui.values[0]);
      	jQuery('.cost_max input').val(ui.values[1]);
      },
      stop: function(){
      	search('');
      }
    });

    jQuery( "#slider-range-s" ).slider({
      range: true,
      min: 0,
      max: 300,
      values: [ 0, 300 ],
      slide: function( event, ui ) {
        jQuery('.s_min input').val(ui.values[0]);
      	jQuery('.s_max input').val(ui.values[1]);
      },
      stop: function(){
      	search('');
      }
    });
}

function phoneNumberButton()
{
	$("#phone_number_btn").click(function () {
		$("#phone_number_btn").hide();
		$("#phone_number_reply").show();
	});
}

function search(url){
	var arg = '';
	if($('#SITE_ID').html() != "" ) url = url + ((url.length > 0) ? "&" : "?") + "SITE_ID=" + $('#SITE_ID').html();
	if(typeof(arguments[1]) != 'undefined'){
		arg = arguments[1];
	}
	$.post('http://realty.bloknot.ru/bn/ajax.php' + url, $('#searchForm').serialize(), function(data){
		$('#searchResult').html(data);
		if(arg){
			$('#searchSelectOrder').val(arg);
		}
	})
}