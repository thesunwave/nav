$(function(){

  $('ul.nav.second > li').hover( function()  {
    $(this).toggleClass("active_nav");
  });


  var menu = function() {
    var $secondMenu = $("#secondMenu");
    var $lis        = $secondMenu.find("li").not('#square-style');
    var width = 0;
    var secondMenuWidth = $secondMenu.outerWidth();
    for(var i = 0; i < $lis.length; i++) {
      width += $lis.eq(i).outerWidth();
      if (width > secondMenuWidth) {
        $lis.eq(i).hide().addClass('hided');
      } else{
        $lis.eq(i).show().removeClass('hided');
      }
    }
    if ($lis.eq($lis.length-1).hasClass('hided') && !$(".zArrow").length){
      $secondMenu.append('<div class="zArrow" style="display:inline-block;"><span class="glyphicon right"></span></div>');
    }
  }

  $(window)
    .on("resize.menu", menu)
    .trigger("resize.menu");

  $(".zArrow").on("click", function(){
    alert(123);
  });

});